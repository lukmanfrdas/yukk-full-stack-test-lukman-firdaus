@extends('layouts.auth.app')

@section('content')
<div class="card shadow" style="width: 37rem;">
    <div class="card-body text-center">
        <img src="{{ asset('assets/media/logos/logo-tigadigit.png') }}" width="200" alt="">
        <img src="{{ asset('assets/media/auth/finish.svg') }}" width="366" alt="">

        <h4 class="card-title mt-8 mb-2 fw-bolder">Registrasi Berhasil</h4>
        <p class="card-text">Silahkan masuk untuk mulai menggunakan tools accounting kami, yang memudahkan bisnis anda</p>

        <div class="d-grid gap-2">
            <a href="{{ route('dashboard') }}" class="btn btn-primary mt-5">Masuk Dashboard</a>
        </div>
    </div>
</div>
@endsection
