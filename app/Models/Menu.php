<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Traits\Uuids;

class Menu extends BaseModel
{
    use HasFactory,Uuids;

    protected $fillable = [
        'name',
        'icon',
        'route',
        'parent_id',
        'permission_id',
    ];

    protected $appends = ['slug'];

    public function getSlugAttribute()
    {
        return Str::slug($this->name, '_');
    }

    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id', 'id');
    }

    public function childrens()
    {
        return $this->hasMany(Menu::class, 'parent_id')->orderBy('name', 'ASC');
    }

    public function permission()
    {
        return $this->belongsTo(Permission::class);
        // return $this->belongsTo(\Spatie\Permission\Models\Permission::class);
    }

    public function role_has_permission()
    {
        if(!empty(Auth::user()))
            return $this->belongsTo(RoleHasPermission::class, 'permission_id', 'permission_id')->where('role_id', @Auth::user()->roles->pluck('id')->first());
        else
            return $this->belongsTo(RoleHasPermission::class, 'permission_id', 'permission_id');
    }
}
