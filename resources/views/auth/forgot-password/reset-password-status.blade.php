@extends('layouts.auth.app')

@section('title', 'Login')

@section('content')
    <div class="card shadow" style="width: 30rem;">
        <div class="card-body text-center">
            <img src="{{ asset('assets/media/auth/hands-hold-mobile-phone-click-checkmark.png') }}" class="fluid mb-3" />
            <h4>Berhasil Ganti Kata Sandi</h4>
            <p>Selamat kata sandi anda telah berhasil di ganti!</p>

            <div class="d-grid gap-2">
                <a href="{{ route('login') }}" class="btn btn-primary mt-5">Masuk</a>
            </div>
        </div>
    </div>

    @include('auth/footer')
@endsection
