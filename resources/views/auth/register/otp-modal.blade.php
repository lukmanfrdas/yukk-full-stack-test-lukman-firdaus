<div class="modal modal-fade" id="otpModal" tabindex="-1" aria-labelledby="otpModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <form action="{{ route('register-company') }}" method="GET">
            <div class="modal-body p-10 text-center">
                <img src="{{ asset('assets/media/logos/logo-tigadigit.png') }}" width="200" alt="">
                <h2 class="fw-bolder mt-10 mb-2">Kode OTP</h2>
                <p style="font-size: 16px;" class="mb-12">Masukkan kode otp yang kami kirimkan</p>

                <div class="otp-field mb-4">
                    <input type="number" />
                    <input class="c bg-secondary-subtle" type="number" disabled />
                    <input class="c bg-secondary-subtle" type="number" disabled />
                    <input class="c bg-secondary-subtle" type="number" disabled />
                    <input class="c bg-secondary-subtle" type="number" disabled />
                    <input class="c bg-secondary-subtle" type="number" disabled />
                </div>

                <p class="resend text-start">
                    Belum dapat kode OTP?<a href="#" class="fw-bolder"> Kirim ulang</a>
                </p>

                <div class="d-grid gap-2">
                    <button type="submit" class="btn btn-primary mt-5 register-btn bg-dark-subtle">Sign In</button>
                </div>
            </div>
        </form>
    </div>
  </div>
</div>

@push('scripts')
<script>
  document.addEventListener("DOMContentLoaded", function() {
    const inputs = document.querySelectorAll(".otp-field > input");
    const button = document.querySelector(".register-btn");

    window.addEventListener("load", () => inputs[0].focus());
    button.setAttribute("disabled", "disabled");

    inputs[0].addEventListener("paste", function (event) {
      event.preventDefault();

      const pastedValue = (event.clipboardData || window.clipboardData).getData(
        "text"
      );
      const otpLength = inputs.length;

      for (let i = 0; i < otpLength; i++) {
        if (i < pastedValue.length) {
          inputs[i].value = pastedValue[i];
          inputs[i].removeAttribute("disabled");
          inputs[i].focus;
        } else {
          inputs[i].value = ""; // Clear any remaining inputs
          inputs[i].focus;
        }
      }
    });

    inputs.forEach((input, index1) => {
      input.addEventListener("keyup", (e) => {
        const currentInput = input;
        const nextInput = input.nextElementSibling;
        const prevInput = input.previousElementSibling;

        if (currentInput.value.length > 1 && currentInput.className === 'c') {
          currentInput.value = "";
          return;
        }

        if (
          nextInput &&
          nextInput.hasAttribute("disabled") &&
          currentInput.value !== ""
        ) {
          nextInput.classList.remove('c')
          nextInput.classList.remove('bg-secondary-subtle')
          nextInput.removeAttribute("disabled");
          nextInput.focus();
        }

        if (e.key === "Backspace") {
          inputs.forEach((input, index2) => {
            if (index1 <= index2 && prevInput) {
              input.setAttribute("disabled", true);
              input.classList.add('c')
              input.classList.add('bg-secondary-subtle')
              input.value = "";
              prevInput.focus();
            }
          });
        }

        button.classList.remove("active");
        button.setAttribute("disabled", true);

        const inputsNo = inputs.length;
        if (!inputs[inputsNo - 1].disabled && inputs[inputsNo - 1].value !== "") {
          button.classList.add("active");
          button.removeAttribute("disabled");

          // All input values can be used
          let allInputValues = inputs[0].value + inputs[1].value + inputs[2].value + inputs[3].value + inputs[4].value + inputs[5].value

          return;
        }
      });
    });
  });

</script>
@endpush
