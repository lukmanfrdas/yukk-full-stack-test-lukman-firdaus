@extends('layouts.app')

@section('pageTitle', $title)

@section('metaTitle', $title)
@section('metaDescription', 'Tiga Digit Indonesia')

@section('breadcrumb')
    <x-global-component.breadcrumb :links="[
        [
            'url' => '#',
            'title' => 'User',
        ],
    ]" />
@endsection

@section('content')

    <div class="row">
        <div class="col-12 mb-4">
            <div class="card card sm">
                <div class="card-body">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $data->name }}</td>
                                <td>{{ $data->balance }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-4 mb-4">
        <div class="col-12">
            <div class="card card-sm">
                <div class="card-body pt-0 pb-0">
                    <form action="{{ route('transaction.store', $data->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-10 fv-row">
                            <label for="category_id" class="form-label">Category</label>
                            <select name="category_id" id="category_id"
                                class="form-control form-control-solid form-select mb-2">
                                <option value="" disabled selected>--Choose option--</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-10 fv-row">
                            <label for="balance" class="required form-label">Balance</label>
                            <input type="number" class="form-control form-control-solid" name="balance"
                                placeholder="Balance" />
                        </div>
                        <button type="submit" class="btn btn-primary btn-hover-scale">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 mb-4">
            <div class="card card sm">
                <div class="card-body">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Category</th>
                                <th>New Balance</th>
                                <th>Out Balance</th>
                                <th>Change Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach ($transactions as $item)
                            <tr>
                                    <td>{{ $item->user->name }}</td>
                                    <td>{{ $item->category->name }}</td>
                                    <td>{{ $item->new_balance }}</td>
                                    <td>{{ $item->out_balance }}</td>
                                    <td>{{ $item->change_balance }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



@endsection

@push('scripts')
@endpush
