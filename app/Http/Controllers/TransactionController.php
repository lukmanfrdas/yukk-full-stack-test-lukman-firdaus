<?php

namespace App\Http\Controllers;

use App\DataTables\TransactionDataTable;
use App\Http\Requests\Transaction;
use App\Models\Category;
use App\Models\Transaction as ModelsTransaction;
use App\Models\User;
use Illuminate\Http\Request;
use View;

class TransactionController extends Controller
{

    protected $model;
    protected $view;

    public function __construct(User $user)
    {
        $this->middleware('can:transaction.list')->only('index');
        $this->middleware('can:transaction.create')->only('store');
        $this->middleware('can:transaction.update')->only('update');
        $this->middleware('can:transaction.delete')->only('destroy');

        $this->model    = $user;
        $this->view     = "pages.transaction";
        $this->route    = "transaction";
        $this->title    = "Transaction";
        $this->categories    = Category::all();

        View::share('route', $this->route);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
        View::share('categories', $this->categories);
    }

    public function index(TransactionDataTable $dataTable)
    {
        return $dataTable->render($this->view . '.index');
    }

    public function show($id)
    {
        $data = User::where('id',$id)->first();
        $transactions = ModelsTransaction::where('user_id', $id)->get();
        return view('pages.transaction.create',compact('data','transactions'));
    }

    public function store(Request $request, $id)
    {
        $input = $request->validate([
            'category_id' => 'required',
            'balance' => 'required',
        ]);

        $user = User::find($id);
        
        $category = Category::where('id',$request->category_id)->first();

        if ($category->status == 1) {
            $inputNewBalance = $input['balance'];
            $modelNewBalance = $user->balance;
            
            $sumNewBalance = $inputNewBalance + $modelNewBalance;
        } else {
            $inputNewBalance = $input['balance'];
            $modelNewBalance = $user->balance;
            
            $sumNewBalance = $modelNewBalance - $inputNewBalance;
        }
        
        $transaction = new ModelsTransaction([
            'user_id' => $id,
            'category_id' => $input['category_id'],
            'change_balance' => $input['balance'],
            'out_change' => $input['balance'],
            'out_balance' => $user->balance
        ]);
        
        
        $transaction->new_balance = $sumNewBalance;
        $transaction->save();
    

        $user->update(['balance' => $sumNewBalance]);

        return back();
    }
}

