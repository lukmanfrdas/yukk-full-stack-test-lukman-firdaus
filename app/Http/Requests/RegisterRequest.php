<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'full_name' => 'required|string|max:255',
            'phone_number' => 'required|numeric|unique:users',
            'company_role_id' => 'required|exists:company_roles,id',
            'password' => 'required|string|min:8',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'full_name.required' => 'Nama lengkap harus diisi',
            'full_name.string' => 'Nama lengkap harus berupa string',
            'full_name.max' => 'Nama lengkap tidak boleh lebih dari :max karakter',
            'phone_number.required' => 'Nomor telepon harus diisi',
            'phone_number.numeric' => 'Nomor telepon harus berupa angka',
            'phone_number.unique' => 'Nomor telepon sudah terdaftar',
            'company_role_id.required' => 'Jabatan perusahaan harus diisi',
            'company_role_id.exists' => 'Jabatan perusahaan tidak valid',
            'password.required' => 'Kata sandi harus diisi',
            'password.string' => 'Kata sandi harus berupa string',
            'password.min' => 'Kata sandi minimal :min karakter',
        ];
    }
}
