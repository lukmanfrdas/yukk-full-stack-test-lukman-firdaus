@extends('layouts.auth.app')

@section('metaTitle', 'Login')

@section('content')
    <div class="card shadow" style="width: 32rem;">
        <div class="card-body text-center">
            {{-- <img src="{{ asset('assets/media/logos/logo-tigadigit.png') }}" width="200" alt=""> --}}
            <h4 class="card-title mt-8 mb-2 fw-bolder">Selamat Datang</h4>
            <p class="card-text">Silahkan lengkapi data untuk masuk</p>

            <form method="POST" action="{{ route('login.submit') }}">
                @csrf
                <div class="mb-3 d-flex flex-column align-items-start">
                    <label for="email" class="form-label fw-bolder">Email</label>
                    <input required type="email" name="email" placeholder="Masukkan email Anda" class="form-control @error('email') is-invalid @enderror"
                        id="email" aria-describedby="emailHelp">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                </div>
                <div class="mb-3 d-flex flex-column align-items-start">
                    <label for="pass" class="form-label fw-bolder">Kata Sandi</label>
                    <div class="input-group input-group-solid mb-5">
                        <input required id="pass" name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Masukan password Anda" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <span class="input-group-text">
                            <i id="toggle-password" class="fa-regular fa-eye-slash"></i>
                        </span>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                    {{-- <div class="form-text mt-4">Lupa kata sandi?
                        <button type="button" data-bs-toggle="modal" data-bs-target="#forgotModal"
                            class="fw-bold text-dark btn btn-link p-0">Ganti</button>
                    </div> --}}
                </div>

                <div class="d-grid gap-2">
                    <button id="btnLogin" disabled type="submit" class="btn btn-primary mt-5">Masuk</button>
                    <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </div>

                <div class="form-text mt-8">Belum memiliki akun? <a href="{{ route('register-form') }}" class="fw-bold">Buat
                        Akun</a></div>
            </form>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="forgotModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('forget.password.get') }}" method="get">
                    <div class="modal-body text-center p-10">
                        {{-- <img src="{{ asset('assets/media/logos/logo-tigadigit.png') }}" width="200" alt=""> --}}
                        <h2 class="fw-bolder mt-10 mb-2">Lupa Kata Sandi</h2>
                        <p style="font-size: 16px; padding: 0 50px;">Masukkan kalimat “<b>Lupa Kata Sandi</b>” dengan benar
                        </p>

                        <div class="mb-3 d-flex flex-column align-items-start">
                            <input type="text" placeholder="Ketik disini" class="form-control confirm">
                        </div>
                        <div class="d-grid gap-2">
                            <button disabled id="btnNext" type="submit" class="btn btn-primary mt-5 next">Lanjut</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const input = document.querySelector(".confirm");
            const button = document.querySelector(".next");

            input.addEventListener("keyup", (e) => {
                const inputVal = e.target.value

                if (inputVal == 'Lupa Kata Sandi') {
                    button.classList.add("active");
                    $('.next').prop('disabled', false)
                } else {
                    button.classList.remove("active");
                    $('.next').prop('disabled', true)
                }
            })
        })

        $(document).ready(function() {
            let showPassword = false;
            setInterval(() => {
                if (showPassword) {
                    $('#pass').prop('type', 'text')
                } else {
                    $('#pass').prop('type', 'password')
                }

                if ($('#email').val() != '' && $('#pass').val() != '') {
                    $('#btnLogin').prop('disabled', false)
                } else {
                    $('#btnLogin').prop('disabled', true)
                }
            }, 100);

            $('#toggle-password').click(function() {
                showPassword = !showPassword
            })
        })
    </script>
@endpush
