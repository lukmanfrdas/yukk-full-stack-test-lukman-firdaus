<!--begin::Modal-->
<div class="modal fade" tabindex="-1" id="user-modal" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog modal-dialog-scrollable">
        <form method="post" action="" class="form needs-validation" id="user-form"
            enctype="multipart/form-data" autocomplete="off" novalidate>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Users</h5>

                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                        aria-label="Close">
                        <span class="svg-icon svg-icon-2x"></span>
                    </div>
                    <!--end::Close-->
                </div>

                <div class="modal-body">

                    <div class="mb-10 fv-row">
                        <label for="role" class="form-label">Role</label>
                        <select name="role" id="role" class="form-control form-control-solid form-select mb-2">
                            <option value="" disabled selected>--Choose option--</option>
                            @foreach ($roles as $role)
                            <option value="{{ $role->name }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="name" class="required form-label">Name</label>
                        <input type="text" class="form-control form-control-solid" name="name" placeholder="Name"
                            required />
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="email" class="required form-label">Email</label>
                        <input type="email" class="form-control form-control-solid" name="email" placeholder="email"
                            required />
                    </div>

                    <div class="mb-10 fv-row">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control form-control-solid" name="password"
                            placeholder="Password" autocomplete="new-password" />
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!--end::Modal-->
