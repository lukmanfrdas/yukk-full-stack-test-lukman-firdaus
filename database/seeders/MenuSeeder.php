<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Menu;
use App\Models\Permission;
use Illuminate\Support\Str;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::beginTransaction();
        try {
            $permission = Permission::whereName('user_management')->first();
            $user_manage = Menu::create([
                'id' => Str::uuid()->toString(),
                'name' => 'User Management',
                'route' => NULL,
                'parent_id' => NULL,
                'permission_id' => $permission->id
            ]);

            $permission = Permission::whereName('data_master')->first();
            $master = Menu::create([
                'id' => Str::uuid()->toString(),
                'name' => 'Data Master',
                'route' => NULL,
                'parent_id' => NULL,
                'permission_id' => $permission->id
            ]);

            $permission = Permission::whereName('transaction')->first();
            $transaction = Menu::create([
                'id' => Str::uuid()->toString(),
                'name' => 'Transaction',
                'route' => NULL,
                'parent_id' => NULL,
                'permission_id' => $permission->id
            ]);

            // user namagement
            $permission = Permission::whereName('user.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'User',
                'route' => 'user.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);

            $permission = Permission::whereName('role.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Role',
                'route' => 'roles.index',
                'parent_id' => $user_manage->id,
                'permission_id' => $permission->id
            ]);
            $permission = Permission::whereName('permission.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Permission',
                'route' => 'permissions.index',
                'parent_id' => $user_manage->id,
                'permission_id' => $permission->id
            ]);


            // master table
            $permission = Permission::whereName('menu.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Menu',
                'route' => 'menus.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);

            $permission = Permission::whereName('category.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Category',
                'route' => 'category.index',
                'parent_id' => $master->id,
                'permission_id' => $permission->id
            ]);

            // transaction table
            $permission = Permission::whereName('transaction.list')->first();
            Menu::Create([
                'id' => Str::uuid()->toString(),
                'name' => 'Transaction',
                'route' => 'transaction.index',
                'parent_id' => $transaction->id,
                'permission_id' => $permission->id
            ]);


            // DB::commit();
        } catch (\Exception $ex) {
            // DB::rollBack();
            dd($ex->getMessage());
        }
    }
}
