<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterGetOtpRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\RegisterVerficationEmailRequest;
use App\Http\Requests\RegisterVerficationOtpRequest;
use App\Mail\MailOtp;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    protected $model;
    protected $view;
    protected $path;

    public function __construct(User $user)
    {
        $this->model    = $user;
        $this->view     = "auth";
    }

    public function loginForm()
    {
        return  view($this->view . ".login.login");
    }

    public function login(LoginRequest $request)
    {
        $input = $request->all();
        unset($input['_token']);
        if (Auth::attempt($input)) {
            $request->session()->regenerate();

            return redirect()->route('dashboard');
        }

        return back()->withErrors([
            'email' => [trans('auth.failed')],
        ])->onlyInput('email');
    }


    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('login');
    }

    public function registerUser(Request $request)
    {
        $validate = $request->validate([
            'email' => 'required|unique:users',
        ]);

        $validate['password'] = bcrypt($request->password);

        User::create($validate);
        return to_route('login')->with('msg', 'Berhasil Membuat Account');
    }

    public function registerForm()
    {
        return view('auth.register.register');
    }


    /**
     * to verify user email address
     *
     * @param RegisterVerficationEmailRequest $request
     * @return Illuminate\Http\JsonResponse
     */
    public function registerVerifyEmail(RegisterVerficationEmailRequest $request)
    {
        $otp = random_int(100000, 999999);

        $createUser = $this->model->create([
            'email' => $request->email,
            'otp' => $otp,
            'joined_date' => now()
        ]);

        $sent = Mail::to($request->email)->send(new MailOtp(['otp' => $otp]));

        if ($sent) {
            return response()->json([
                'status' => 'success',
                'message' => 'OTP has been sent to your email'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to send OTP to your email'
            ]);
        }
    }

    /**
     * to get new otp when user click resend otp
     *
     * @param RegisterVerficationEmailRequest $request
     * @return Illuminate\Http\JsonResponse
     */
    public function registerGetOtp(RegisterGetOtpRequest $request)
    {
        $otp = random_int(100000, 999999);

        $updateOtp = $this->model->where([
            'email' => $request->email,
        ])->update([
            'otp' => $otp
        ]);

        $sent = Mail::to($request->email)->send(new MailOtp(['otp' => $otp]));

        if ($sent) {
            return response()->json([
                'status' => 'success',
                'message' => 'OTP has been sent to your email'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to send OTP to your email'
            ]);
        }
    }

    /**
     * to verify otp
     *
     * @param RegisterVerficationOtpRequest $request
     * @return Illuminate\Http\JsonResponse
     */
    public function registerVerifyOtp(RegisterVerficationOtpRequest $request){
        $user = $this->model->where([
            'email' => $request->email,
            'otp' => $request->otp
        ])->first();

        if($user){
            Auth::loginUsingId($user->id, true);
            return response()->json([
                'status' => 'success',
                'message' => 'OTP is valid'
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'OTP Tidak sesuai',
                'errors' => [
                    'otp' => ['OTP tidak sesuai']
                ]
            ], 422);
        }
    }

    public function registerComplete()
    {
        return view('auth.register.register-complete');
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->model->find(Auth::id());
        $user->name = $request->full_name;
        $user->phone_number = $request->phone_number;
        $user->company_role_id = $request->company_role_id;
        $user->password = Hash::make($request->password);
        $user->username = Auth::user()->email;
        $user->type = 'admin-office';
        $user->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Register success'
        ]);
    }
}
