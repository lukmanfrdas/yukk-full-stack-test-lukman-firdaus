<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body text-center p-10">
        <img src="{{ asset('assets/media/auth/registered-email.svg') }}" width="200" alt="">
        <h4 class="fw-bolder my-5">Email Sudah Terdaftar</h4>
        <p style="font-size: 16px; padding: 0 50px;">Email ini sudah ter-registrasi silahkan lakukan sign in untuk masuk</p>

        <div class="d-grid gap-2">
          <a href="{{ route('login') }}" class="btn btn-primary mt-5">Masuk</a>
        </div>
      </div>
    </div>
  </div>
</div>
