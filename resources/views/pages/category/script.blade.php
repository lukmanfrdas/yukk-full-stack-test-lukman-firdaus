<script>
    $(document).ready(function() {
        let method = 'POST'

        //STORE
        $('#category-form').submit(function(e) {
            e.preventDefault()
            const payload = $(this).serialize()
            const url = $(this).attr('action')
            const validator = document.getElementById('category-form').checkValidity()
            if (!validator) {
                clearValidator()
                formValidator()
            } else {
                $.ajax({
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        method: method, //UNTUK DINAMIS METHOD
                        data: payload,
                    })
                    .done(response => {
                        if (response.success) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                text: response.message
                            })

                            $('.dataTable').DataTable().ajax.reload()
                            $('#category-modal').modal('hide')
                        }
                        clearValidator()
                    })
                    .fail(response => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: response.responseJSON.message
                        })
                    })
            }
        })
        // validator
        function formValidator() {
            let valid = true
            $('input[required], select[required]').each(function() {
                if (!this.checkValidity()) {
                    valid = false
                    $(this).addClass('is-invalid')
                    $(this).after('<div class="invalid-feedback">' + this.validationMessage + '</div>')
                }
            })
            return valid;
        }

        function clearValidator() {
            $('.invalid-feedback').remove()
            $('input[required], select[required]').removeClass('is-invalid')
        }

        //DELETE
        $(document).on('click', '.delete-btn', function() {
            Swal.fire({
                title: 'Are you sure',
                icon: 'info',
                showDenyButton: true,
                confirmButtonText: 'Yes',
                denyButtonText: `No`,
            }).then((result) => {
                if (result.isConfirmed) {
                    const url = '{{ url('category') }}/' + $(this).data('id')
                    $.ajax({
                        url: url,
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    }).done(response => {
                        console.log(response)
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            message: response.message
                        })
                        $('.dataTable').DataTable().ajax.reload()
                    })
                }
            })
        })

        //EDIT
        $(document).on('click', '.edit-btn', function() {
            const url = '{{ url('category') }}/' + $(this).data('id')
            $('#category-form').attr('action', url)
            method = 'PUT'

            let data = {}
            $.get(url).done(response => {
                if (response.success) data = response.data
                setForm(data)
                $('#category-modal').modal('show')
            })
        })

        function setForm(data) {
            $('input[name=name]').val(data.name)
            $('select[name=status]').val(data.status).change()
        }
        function clearForm() {
            $('input[name=name]').val('')
            $('select[name=status]').val('').change()
        }

        $('.modal').on('hidden.bs.modal', function(event) {
            $('#category-form').attr('action', '{{ route('category.store') }}')
            //hapus gambar ketika modal ditutup
            clearForm()
            $('.btn-cancel-img').click()
            method = 'POST'
            $('.image-input-empty').attr('style', {"background-color": "#fff"})
        })

    })
</script>
