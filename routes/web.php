<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;

// user management
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;

// authentication
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\ProfileController;

// data env
use App\Http\Controllers\MenuController;

// data master
use App\Http\Controllers\AdjustmentTypeController;
use App\Http\Controllers\BusinessInformationController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CashAndBankController;
use App\Http\Controllers\CashAndBankSubcategoryController;
use App\Http\Controllers\ChartOfAccountController;
use App\Http\Controllers\CommodityCategoryController;
use App\Http\Controllers\CurrencyController;
use App\Http\Controllers\CostTypeOtherController;
use App\Http\Controllers\SpecialNotificationBarController;
use App\Http\Controllers\FormStyleController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\InventoryTypeController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductionCostsTypeController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\DiscountTypeController;
use App\Http\Controllers\TaxTypesController;
use App\Http\Controllers\AssetCategoryController;
use App\Http\Controllers\BannersTutorialController;
use App\Http\Controllers\PromotionalBannersController;
use App\Http\Controllers\TaxesController;
use App\Http\Controllers\SolutionListController;
use App\Http\Controllers\AssetsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UnitsController;
use App\Http\Controllers\SampleTransactionController;
use App\Http\Controllers\CompanyCustomerController;
use App\Http\Controllers\ShopController;


use App\Http\Controllers\CommoditySecurityController;
use App\Http\Controllers\CompanyCategoryController;
use App\Http\Controllers\CompanyRoleController;
use App\Http\Controllers\MarketingChannelController;
use App\Http\Controllers\NumberOfEmployeeController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\TransactionTagController;



// transaction
use App\Http\Controllers\TransactionController;
use App\Http\Requests\SolutionListRequest;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// Authentications
Route::get('/login', [AuthController::class, 'loginForm'])->name('login');
Route::post('/login-submit', [AuthController::class, 'login'])->name('login.submit');
Route::get('/register', [AuthController::class, 'registerForm'])->name('register-form');
Route::post('/register/verify-email', [AuthController::class, 'registerVerifyEmail'])->name('register.verify-email');
Route::post('/register/get-otp', [AuthController::class, 'registerGetOtp'])->name('register.get-otp');
Route::post('/register/verify-otp', [AuthController::class, 'registerVerifyOtp'])->name('register.verify-otp');
Route::post('/register/user', [AuthController::class, 'registerUser'])->name('register.user');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout.user');

// forgot password
Route::get('forget-password', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');
Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post');
Route::get('reset-password-status/{status}', [ForgotPasswordController::class, 'resetPasswordStatus'])->name('reset.password.status');
Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password/{token}', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');

Route::middleware(['auth'])->group(function () {
    Route::get('/register/complete', [AuthController::class, 'registerComplete'])->name('register.complete');
    Route::post('/register', [AuthController::class, 'register'])->name('register');

    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

    Route::resource('user', UserController::class)->except(['update']);
    Route::post('user/{id}', [UserController::class, 'update'])->name('user.update');
    Route::post('roles/{role}', [RoleController::class, 'update'])->name('admin.roles.update');
    Route::resource('roles', RoleController::class);
    Route::resource('permissions', PermissionController::class);

    // data env
    Route::resource('menus', MenuController::class);

    // datamaster
    Route::resource('category', CategoryController::class);

    Route::get('transaction', [TransactionController::class, 'index'])->name('transaction.index');
    Route::get('transaction/{id}', [TransactionController::class, 'show'])->name('transaction.show');
    Route::post('transaction/{id}', [TransactionController::class, 'store'])->name('transaction.store');




    //profile
    Route::get('profile', [ProfileController::class , 'index'])->name('profile.index');

});
