<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bahasa Pesan Autentikasi
    |--------------------------------------------------------------------------
    |
    | Baris bahasa berikut digunakan selama proses autentikasi untuk berbagai
    | pesan yang perlu kami tampilkan kepada pengguna. Anda bebas untuk memodifikasi
    | baris bahasa ini sesuai dengan kebutuhan aplikasi Anda.
    |
    */

    'failed' => 'Kredensial ini tidak cocok dengan data kami.',
    'password' => 'Kata sandi yang diberikan salah.',
    'throttle' => 'Terlalu banyak percobaan login. Silakan coba lagi dalam :seconds detik.',

];
