<?php
namespace App\DataTables;

use App\Models\Menu;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MenuDataTable extends DataTable
{

    protected $model;
    protected $view;

    public function __construct(){
        $this->view     = "menu";
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('parent_name', function($query) { 
                return @$query->parent->name; 
            })
            ->addColumn('permission', function($query) { 
                return @$query->permission->name; 
            })
            ->addColumn('icon', function($query) { 
                return '<span class="menu-icon"><i class="ki-outline '.$query->icon.' fs-2"></i></span>';
            })
            ->addColumn('action', "pages.".$this->view.'.action')
            ->rawColumns(['icon', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Menu $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Menu $model)
    {
        return $model->select('permissions.name as permission_name', 'menus.*')
        ->leftjoin('permissions', 'permissions.id', '=', 'menus.permission_id')
        ->orderBy('created_at', 'desc')
        ->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('menus-table')
                    ->addTableClass('table table-striped table-bordered table-hover')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(0)
                    ->buttons(['export']);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('icon'),
            Column::make('route'),
            Column::make('parent_name'),
            Column::make('permission_name'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }


}
