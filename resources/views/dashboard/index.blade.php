@extends('layouts.app')
@section('removePageToolbar', true)

@section('content')
    <div class="dashboard">
        <div class="row mt-5">
            <div class="col-md-8 col-sm-12">
                <cash-flow-banking-chart />
            </div>
        </div>
    </div>
@endsection