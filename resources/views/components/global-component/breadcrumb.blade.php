<div>
    <ol class="breadcrumb breadcrumb-dot text-muted fs-6 fw-semibold">
        <li class="breadcrumb-item"><a href="{{ url('/') }}" class="">Home</a></li>

        @foreach ($links ?? [] as $index => $link)
            @if ($index === count($links) - 1)
                <li class="breadcrumb-item text-muted">{{ $link['title'] }}</li>
            @else
                <li class="breadcrumb-item"><a href="{{ $link['url'] }}" class="">{{ $link['title'] }}</a></li>
            @endif
        @endforeach
    </ol>
</div>
