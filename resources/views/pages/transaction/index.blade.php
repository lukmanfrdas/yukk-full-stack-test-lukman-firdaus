@extends('layouts.app')

@section('pageTitle', $title)

@section('metaTitle', $title)
@section('metaDescription', 'Tiga Digit Indonesia')

@section('breadcrumb')
    <x-global-component.breadcrumb :links="[
        [
            'url' => '#',
            'title' => 'Transaction',
        ],
    ]" />
@endsection

@section('content')

    <div class="row">
        <form action="{{ route('transaction.index') }}" method="GET" class="row g-3">
            @csrf
            <div class="col-md-4">
                <div class="mb-10 fv-row">
                    <label for="start_date" class="required form-label">Start Date</label>
                    <input type="date" class="form-control form-control-solid" name="start_date" placeholder=""
                        value="{{ request('start_date') }}" required />
                </div>
            </div>
            <div class="col-md-4">
                <div class="mb-10 fv-row">
                    <label for="end_date" class="required form-label">End Date</label>
                    <input type="date" class="form-control form-control-solid" name="end_date" placeholder=""
                        value="{{ request('end_date') }}" required />
                </div>
            </div>
            <div class="col-md-2">
                <div class="mt-7 fv-row">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div>


    <div class="row">
        <div class="col-12">
            <div class="card card-sm">
                <div class="card-body pt-0 pb-0">
                    <div class="table-responsive">
                        {{ $dataTable->table() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{ $dataTable->scripts() }}
    @include($view . '.script')
@endpush
