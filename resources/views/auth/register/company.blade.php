@extends('layouts.auth.app')

@section('content')
<div class="card shadow" style="width: 32rem;">
    <div class="card-body text-center">
        <img src="{{ asset('assets/media/logos/logo-tigadigit.png') }}" width="200" alt="">
        <h4 class="card-title mt-8 mb-2 fw-bolder">Isi Data Perusahaan</h4>
        <p class="card-text">Sesuaikan data dengan keperluan perusahaan</p>

        <form class="mt-10" action="{{ route('register-preference') }}" method="GET">
            <div class="mb-3 d-flex flex-column align-items-start">
                <label for="company_name" class="form-label fw-bolder">Nama Perusahaan</label>
                <input type="text" placeholder="Masukkan nama perusahaan" class="form-control" id="company_name">
            </div>
            <div class="mb-3 d-flex flex-column align-items-start">
                <label class="form-label fw-bolder">Kategori Perusahaan</label>
                <select class="form-select" aria-label="Default select example">
                    <option selected>Pilih Kategori</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
            <div class="mb-3 d-flex flex-column align-items-start">
                <label class="form-label fw-bolder">Jumlah Karyawan</label>
                <select class="form-select" aria-label="Default select example">
                    <option selected>Masukkan jumlah</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
            <div class="mb-3 d-flex flex-column align-items-start">
                <label class="form-label fw-bolder">Siapa Customer Anda</label>
                <select class="form-select" aria-label="Default select example">
                    <option selected>Pilih customer</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>

            <div class="d-grid gap-2">
                <button type="submit" class="btn btn-primary mt-5">Lanjutkan</button>
            </div>
        </form>
    </div>
</div>
@endsection
