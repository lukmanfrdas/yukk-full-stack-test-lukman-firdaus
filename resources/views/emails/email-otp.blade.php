@extends('emails.layout')
@section('content')
    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td style="padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #000000;">
                <h4>Tigadigit - Kode Verifikasi</h4>
                <p style="margin: 0 0 10px;">
                    Selangkah menuju akun Tigadigit bisnis anda!<br/><br/>
                    Berikut kode OTP Anda: <strong>{{ $otp }}</strong><br/><br/>
                    Silakan masukkan kode OTP di atas melalui platform Tigadigit untuk melanjutkan penggunaan aplikasi.
                </p>
            </td>
        </tr>
    </table>
@endsection
