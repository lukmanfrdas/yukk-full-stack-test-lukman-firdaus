<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bahasa Pesan Paginasi
    |--------------------------------------------------------------------------
    |
    | Baris bahasa berikut digunakan oleh perpustakaan paginasi untuk membangun
    | tautan paginasi sederhana. Anda bebas mengubahnya menjadi apa pun
    | yang Anda inginkan untuk menyesuaikan tampilan Anda agar lebih sesuai dengan aplikasi Anda.
    |
    */

    'previous' => '&laquo; Sebelumnya',
    'next' => 'Selanjutnya &raquo;',

];
