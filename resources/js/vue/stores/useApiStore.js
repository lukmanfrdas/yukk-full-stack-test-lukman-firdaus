import { defineStore } from 'pinia'

export const useApiStore = defineStore('apiStore', {
  state: () => ({
    config: {
      loading: true,
    },
    token: '',
    errors: null,
    showLoading: false,
    dialog: false,
    dialogTitle: '',
    dialogMessage: ''
  }),

  getters: {

  },

  actions: {

  }
})
