<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;

use Illuminate\Support\Str;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permissions
        $createPermissions = [
            ['name'=>'data_master','guard_name'=>'web'],
            ['name'=>'user_management','guard_name'=>'web'],
            ['name'=>'transaction','guard_name'=>'web'],

            ['name'=>'menu.list','guard_name'=>'web'],
            ['name'=>'menu.create','guard_name'=>'web'],
            ['name'=>'menu.update','guard_name'=>'web'],
            ['name'=>'menu.delete','guard_name'=>'web'],

            ['name' => 'role.list', 'guard_name' => 'web'],
            ['name' => 'role.create', 'guard_name' => 'web'],
            ['name' => 'role.update', 'guard_name' => 'web'],
            ['name' => 'role.delete', 'guard_name' => 'web'],

            ['name' => 'permission.list', 'guard_name' => 'web'],
            ['name' => 'permission.create', 'guard_name' => 'web'],
            ['name' => 'permission.update', 'guard_name' => 'web'],
            ['name' => 'permission.delete', 'guard_name' => 'web'],

            ['name' => 'user.list', 'guard_name' => 'web'],
            ['name' => 'user.create', 'guard_name' => 'web'],
            ['name' => 'user.update', 'guard_name' => 'web'],
            ['name' => 'user.delete', 'guard_name' => 'web'],

            ['name' => 'transaction.list', 'guard_name' => 'web'],
            ['name' => 'transaction.create', 'guard_name' => 'web'],
            ['name' => 'transaction.update', 'guard_name' => 'web'],
            ['name' => 'transaction.delete', 'guard_name' => 'web'],

            ['name' => 'category.list', 'guard_name' => 'web'],
            ['name' => 'category.create', 'guard_name' => 'web'],
            ['name' => 'category.update', 'guard_name' => 'web'],
            ['name' => 'category.delete', 'guard_name' => 'web'],
        ];

        $role_superadmin = Role::whereName('superadmin')->first();
        foreach($createPermissions as $permission){
            $data = Permission::where('name', $permission['name'])
            ->where('guard_name', $permission['guard_name'])
            ->first();

            if (is_null($data)){
                $item = Permission::create([
                    'id' => Str::uuid()->toString(),
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ]);
                $role_superadmin->givePermissionTo($item->name);
            }

        }



    }
}
