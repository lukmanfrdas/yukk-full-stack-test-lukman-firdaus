@extends('layouts.app')

@section('pageTitle', $title)   

@section('metaTitle', $title)
@section('metaDescription', 'Tiga Digit Indonesia')

@section('breadcrumb')
    <x-global-component.breadcrumb :links="[
        [
            'url' => '#',
            'title' => 'Menu',
        ],
    ]" />
@endsection

@section('actionPage')
    <button type="button" class="btn btn-primary btn-hover-scale" data-bs-toggle="modal" data-bs-target="#menu-modal">
        Create
    </button>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-sm">
                <div class="card-body pt-0 pb-0">
                    <div class="table-responsive">
                        {{ $dataTable->table() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include($view . '.form')
@endsection

@push('scripts')
    {{ $dataTable->scripts() }}
    @include($view . '.script')
@endpush
