import { defineStore } from 'pinia'

export const useInitStore = defineStore('initStore', {
  state: () => ({
    csrf: '',
  }),

  getters: {

  },

  actions: {

  }
})
