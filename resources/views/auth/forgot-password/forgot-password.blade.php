@extends('layouts.auth.app')

@section('title', 'Login')

@section('content')
    <div class="card shadow" style="width: 32rem;">
        <div class="card-body">
            <div class="text-center">
                <img src="{{ asset('assets/media/logos/logo-tigadigit.png') }}" width="200" alt="">
                <h4 class="card-title mt-8 mb-2 fw-bolder">Lupa Kata Sandi</h4>
                <p class="card-text">Masukkan email akun Anda</p>
            </div>
            <form method="post" action="{{ route('forget.password.post') }}">
                @csrf
                <div class="mb-3 d-flex flex-column align-items-start">
                    <label for="email" class="form-label fw-bolder">Email</label>
                    <input type="email" name="email" placeholder="Masukkan email Anda"
                        class="form-control @error('email') is-invalid @enderror" id="email"
                        aria-describedby="emailHelp">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="d-grid gap-2">
                    <button id="btnLogin" type="submit" class="btn btn-primary mt-5">Lanjut</button>
                    <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </div>

                <div class="form-text mt-8 text-center">Ingin coba login ulang? <a href="{{ route('login') }}" class="fw-bold">Masuk</a>
                </div>
            </form>
        </div>
    </div>

    @include('auth/footer')
@endsection


@push('scripts')
    <script>
        $(document).ready(function() {
            setInterval(() => {
                if ($('#email').val() != '') {
                    $('#btnLogin').prop('disabled', false)
                } else {
                    $('#btnLogin').prop('disabled', true)
                }
            }, 100);
        })
    </script>
@endpush
