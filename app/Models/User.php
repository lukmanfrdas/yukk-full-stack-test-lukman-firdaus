<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;
use App\Traits\Uuids;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, softDeletes, Uuids, HasRoles;
    protected $fillable = [
        'name',
        'email',
        'password',
        'type',
        'balance',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $hidden = [
        // 'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()}))
                $model->{$model->getKeyName()} = Str::uuid()->toString();
            if (Schema::hasColumn($model->getTable(), 'created_by'))
                $model->created_by = Auth::id() ?? 'guest';
        });

        static::updating(function ($model) {
            if (Schema::hasColumn($model->getTable(), 'updated_by'))
                $model->updated_by = Auth::id() ?? 'guest';
        });

        static::deleting(function ($model) {
            if (Schema::hasColumn($model->getTable(), 'deleted_by')) {
                $model->deleted_by = Auth::id() ?? 'guest';
                $model->save();
            }
        });
    }

}
