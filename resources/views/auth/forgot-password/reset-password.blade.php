@extends('layouts.auth.app')

@section('title', 'Login')

@section('content')
    <div class="card shadow" style="width: 32rem;">
        <div class="card-body">
            <div class="text-center">
                <img src="{{ asset('assets/media/logos/logo-tigadigit.png') }}" width="200" alt="">
                {{-- <h4 class="card-title mt-8 mb-2 fw-bolder">Rest Kata Sandi</h4> --}}
                <p class="card-text">Reset kata sandi</p>
            </div>
            <form method="POST" action="{{ route('reset.password.post', ['token' => $token]) }}">
                @csrf
                <div class="mb-3 d-flex flex-column align-items-start">
                    <label for="password" class="form-label fw-bolder">Kata sandi baru</label>
                    <input type="password" name="password" placeholder="********"
                        class="form-control @error('password') is-invalid @enderror" id="password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="mb-3 d-flex flex-column align-items-start">
                    <label for="cpassword" class="form-label fw-bolder">Konfirmasi kata sandi</label>
                    <input type="password" name="password_confirm" placeholder="********"
                        class="form-control @error('password_confirm') is-invalid @enderror" id="cpassword">
                    @error('password_confirm')
                        <span class="invalid-feedback" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="d-grid gap-2">
                    <button id="btnLogin" type="submit" class="btn btn-primary mt-5">Konfirmasi</button>
                    <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </div>
            </form>
        </div>
    </div>

    @include('auth/footer')
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            setInterval(() => {
                if ($('#password').val() != '' && $('#cpassword').val() != '') {
                    $('#btnLogin').prop('disabled', false)
                } else {
                    $('#btnLogin').prop('disabled', true)
                }
            }, 100);
        })
    </script>
@endpush
