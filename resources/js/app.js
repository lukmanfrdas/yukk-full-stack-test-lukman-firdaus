import './bootstrap';

import { createApp } from 'vue/dist/vue.esm-bundler.js';
import { createPinia } from 'pinia'

import GlobalInit from './vue/components/GlobalInit.vue';

import HelpFeature from './vue/components/dashboard/HelpFeature.vue';
import PackagePlan from './vue/components/dashboard/PackagePlan/PackagePlan.vue';
import RevenueTotal from './vue/components/dashboard/RevenueTotal.vue';
import ExpenditureTotal from './vue/components/dashboard/ExpenditureTotal.vue';
import ProfitTotal from './vue/components/dashboard/ProfitTotal.vue';
import ProcurementChart from './vue/components/dashboard/ProcurementChart.vue';
import ExpenditureTotalChart from './vue/components/dashboard/ExpenditureTotalChart.vue';
import TopCustomers from './vue/components/dashboard/TopCustomers.vue';
import CashFlowBankingChart from './vue/components/dashboard/CashFlowBankingChart.vue';
import TopProducts from './vue/components/dashboard/TopProducts.vue';

import AssetsPayableChart from './vue/components/dashboard/AssetsPayableChart.vue';
import GrossMarginChart from './vue/components/dashboard/GrossMarginChart.vue';
import OperatingMarginChart from './vue/components/dashboard/OperatingMarginChart.vue';
import NetMarginChart from './vue/components/dashboard/NetMarginChart.vue';

import CashFlowChart from './vue/components/dashboard/CashFlowChart.vue';
import Ad from './vue/components/dashboard/Ad.vue';

import TopSuppliers from './vue/components/dashboard/TopSuppliers.vue';

//register
import Register from './vue/components/register/Register.vue'
import RegisterEmail from './vue/components/register/RegisterEmail.vue'

const app = createApp({
    components: {
        GlobalInit,
        HelpFeature,
        PackagePlan,
        RevenueTotal,
        ExpenditureTotal,
        ProfitTotal,
        ProcurementChart,
        ExpenditureTotalChart,
        TopCustomers,
        CashFlowBankingChart,
        TopProducts,
        AssetsPayableChart,
        GrossMarginChart,
        OperatingMarginChart,
        NetMarginChart,
        CashFlowChart,
        Ad,
        TopSuppliers,

        Register,
        RegisterEmail
    }
});

const pinia = createPinia()
app.use(pinia)
app.mount('#kt_app_root');
