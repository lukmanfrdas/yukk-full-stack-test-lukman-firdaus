<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class ProfileController extends Controller
{
    protected $model;
    protected $view;

    /**
     * constructor
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->middleware('can:permission.list')->only('index');
        $this->middleware('can:permission.create')->only('store');
        $this->middleware('can:permission.update')->only('update');
        $this->middleware('can:permission.delete')->only('destroy');

        $this->model    = $model;
        $this->view     = "pages.profile";
        $this->route    = "profile";
        $this->title    = "Profile";

        View::share('route', $this->route);
        View::share('view', $this->view);
        View::share('model', $this->model);
        View::share('title', $this->title);
    }

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $data = $this->model->find(Auth::user()->id);
        return view("{$this->view}.index", compact('data'));
    }
}
