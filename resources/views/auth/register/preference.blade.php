@extends('layouts.auth.app')

@section('content')
<div class="card shadow" style="width: 32rem;">
    <div class="card-body text-center">
        <img src="{{ asset('assets/media/logos/logo-tigadigit.png') }}" width="200" alt="">
        <h4 class="card-title mt-8 mb-2 fw-bolder">Isi Data Perusahaan</h4>
        <p class="card-text">Sesuaikan data dengan keperluan perusahaan</p>

        <form class="mt-10" action="{{ route('register-finish') }}" method="GET">
            <div class="mb-3 d-flex flex-column align-items-start">
                <label class="form-label fw-bolder">Dari Mana Anda Tau Tigadigit?</label>
                <select class="form-select" aria-label="Default select example">
                    <option selected>Pilih salah satu</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
            <div class="mb-3 d-flex flex-column align-items-start">
                <label class="form-label fw-bolder">Apa Yang Anda Butuhkan Dari Tigadigit?</label>
                <select class="form-select" aria-label="Default select example">
                    <option selected>Pilih salah satu</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>

            <div class="d-grid gap-2">
                <button type="submit" class="btn btn-primary mt-5">Konfirmasi</button>
            </div>
        </form>
    </div>
</div>
@endsection
