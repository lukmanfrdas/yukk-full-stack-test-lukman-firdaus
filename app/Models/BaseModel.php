<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class BaseModel extends Model
{
    use SoftDeletes;

    protected $fillable= [
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    //automaticaly set by auth user id
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
        if (empty($model->{$model->getKeyName()})) $model->{$model->getKeyName()} = Str::uuid()->toString();
            if (Schema::hasColumn($model->getTable(), 'created_by')) $model->created_by = Auth::id() ?? 'system';
        });

        static::updating(function ($model) {
            if (Schema::hasColumn($model->getTable(), 'updated_by')) $model->updated_by = Auth::id() ?? 'system';
        });

        static::deleting(function ($model) {
            if (Schema::hasColumn($model->getTable(), 'deleted_by')) {
                $model->deleted_by = Auth::id() ?? 'system';
                $model->save(); // Save to update the `deleted_by` column
            }
        });
    }

}
