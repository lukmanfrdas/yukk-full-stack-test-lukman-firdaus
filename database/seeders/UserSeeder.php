<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            $permissions = [
                'transaction',
                'user_management',
                
                'role.list',
                'role.create',
                'role.update',
                'role.delete',

                'permission.list',
                'permission.create',
                'permission.update',
                'permission.delete',

                'user.list',
                'user.create',
                'user.update',
                'user.delete',

                'category.list',
                'category.create',
                'category.update',
                'category.delete',

                'transaction.list',
                'transaction.create',
                'transaction.update',
                'transaction.delete',

                'menu.list',
                'menu.create',
                'menu.update',
                'menu.delete',
            ];

            $roles = [
                'superadmin',
            ];
            
            $role_superadmin = Role::whereName('superadmin')->first();
            $role_superadmin->givePermissionTo($permissions);
            $user = User::create([
                'name' => 'Super Admin',
                'email' => 'superadmin@admin.com',
                'type' => 'superadmin',
                'password' => Hash::make('superadmin123'),
            ]);
            $user->assignRole('superadmin');

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex->getMessage());
        }


    }
}
