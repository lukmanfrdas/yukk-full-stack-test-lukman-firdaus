<?php

return [
    'name' => env('APP_NAME', 'TigaDigit'),
    'url' => env('APP_URL', 'http://localhost'),
    'meta_description' => 'Tiga digit',
    'meta_keywords' => 'tiga digit, financial',
];
